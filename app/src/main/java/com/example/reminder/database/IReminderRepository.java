package com.example.reminder.database;
import com.example.reminder.model.Reminder;

import java.util.ArrayList;

public interface IReminderRepository {

    public String addReminder(String title, String date, String time);
    public ArrayList<Reminder> getAllReminders();
    public boolean removeReminder(String title);
    public String updateReminder(Reminder reminder);
}
