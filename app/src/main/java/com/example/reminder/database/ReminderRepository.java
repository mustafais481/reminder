package com.example.reminder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.reminder.model.Reminder;

import java.util.ArrayList;

public class ReminderRepository implements IReminderRepository{
    private DataBaseManager dataBaseManager;
    private static ReminderRepository instance;
    public ReminderRepository(Context context)
    {
        this.dataBaseManager = DataBaseManager.getInstance(context);
    }

    public static ReminderRepository getInstance(Context context){
        if(instance==null){
            instance= new ReminderRepository(context);
        }
        return instance;
    }
    @Override
    public String addReminder(String title, String date, String time) {
        SQLiteDatabase database = this.dataBaseManager.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", title);
        contentValues.put("date", date);
        contentValues.put("time", time);

        float result = database.insert("reminder", null, contentValues);

        if (result == -1) {
            return "Failed";
        } else {
            return "Successfully inserted";
        }
    }

    @Override
    public ArrayList<Reminder> getAllReminders() {
        ArrayList<Reminder> reminders = new ArrayList<>();

        Cursor cursor = this.dataBaseManager.getReadableDatabase()
                .rawQuery("select * from reminder order by id desc", null);

        while (cursor.moveToNext()) {
            Reminder model = new Reminder(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
            reminders.add(model);
        }

        return reminders;
    }

    @Override
    public boolean removeReminder(String title) {
        String[] string = {String.valueOf(title)};
        long line = this.dataBaseManager.getWritableDatabase().delete("reminder", "title=?", string);
        return line != 0;
    }

    @Override
    public String updateReminder(Reminder reminder) {
        String[] string = {String.valueOf(reminder.getId())};
        SQLiteDatabase database = this.dataBaseManager.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", reminder.getTitle());
        contentValues.put("date", reminder.getDate());
        contentValues.put("time", reminder.getTime());

        float result = database.update("reminder", contentValues, "id=?", string);

        if (result == -1) {
            return "Failed";
        } else {
            return "Successfully updated";
        }
    }
}
