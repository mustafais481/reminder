package com.example.reminder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DataBaseManager extends SQLiteOpenHelper {

    private static final int CURRENT_DB_VERSION = 1;
    private static String dbname = "ReminderDB";
    private  static DataBaseManager instance;
    public DataBaseManager(@Nullable Context context) {
        super(context, dbname, null, CURRENT_DB_VERSION);
    }

    public static DataBaseManager getInstance(Context context) {
        if(instance==null){
            instance= new DataBaseManager(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query = "create table reminder(id integer primary key autoincrement,title text,date text,time text)";
        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        String query = "DROP TABLE IF EXISTS reminder";
        sqLiteDatabase.execSQL(query);
        onCreate(sqLiteDatabase);

    }

}
