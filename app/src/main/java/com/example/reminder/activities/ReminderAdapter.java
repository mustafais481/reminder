package com.example.reminder.activities;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reminder.R;
import com.example.reminder.database.ReminderRepository;
import com.example.reminder.model.IOnSelectReminder;
import com.example.reminder.model.Reminder;

import java.util.ArrayList;

public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.myviewholder> implements IOnSelectReminder {
    ArrayList<Reminder> reminders;
    public ReminderAdapter(ArrayList<Reminder> dataholder) {
        this.reminders = dataholder;
    }

    @NonNull
    @Override
    public myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_reminder_file, parent, false);
        return new myviewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myviewholder holder, int position) {
        holder.mTitle.setText(reminders.get(position).getTitle());
        holder.mDate.setText(reminders.get(position).getDate());
        holder.mTime.setText(reminders.get(position).getTime());
        holder.setReminder(reminders.get(position));
        holder.setListener(this);

    }

    @Override
    public int getItemCount() {
        return reminders.size();
    }

    @Override
    public void onSelectReminder(Reminder reminder, Context context) {
        ReminderRepository.getInstance(context).removeReminder(reminder.getTitle());
        reminders = ReminderRepository.getInstance(context).getAllReminders();
        notifyDataSetChanged();
        Toast.makeText(context, reminder.getTitle() + " successfully deleted", Toast.LENGTH_SHORT).show();
    }

    class myviewholder extends RecyclerView.ViewHolder {

        TextView mTitle, mDate, mTime;
        Reminder reminder;
        IOnSelectReminder listener;

        public void setReminder(Reminder reminder) {
            this.reminder = reminder;
        }

        public void setListener(IOnSelectReminder listener) {
            this.listener = listener;
        }

        public myviewholder(@NonNull View itemView) {
            super(itemView);

            mTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            mDate = (TextView) itemView.findViewById(R.id.txtDate);
            mTime = (TextView) itemView.findViewById(R.id.txtTime);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listener.onSelectReminder(reminder, v.getContext());
                    notifyDataSetChanged();
                    return true;
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), "Update reminder", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(v.getContext(), UpdateReminderActivity.class);
                    intent.putExtra("reminderSelected", reminder);
                    v.getContext().startActivity(intent);

                }
            });
        }
    }
}
